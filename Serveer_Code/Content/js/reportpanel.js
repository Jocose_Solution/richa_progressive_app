﻿
$(function () {
    BindSalesReport($("#FromDate").val(), $("#ToDate").val());
    BindOTPReport($("#Agencyid").val());
});

function openTab(evt, sectionName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(sectionName).style.display = "block";
    evt.currentTarget.className += " active";

    if (sectionName == "Sales") {
        $("#FromDate").val(""), $("#ToDate").val("");
        BindSalesReport($("#FromDate").val(), $("#ToDate").val());
    }
    else if (sectionName == "Agent") {
        BindAgentReport();
    }
    else if (sectionName == "Airlines") {
        BindAirlinesReport();
    }
    else if (sectionName == "Sector") {
        BindSectorReport();
    }
    else if (sectionName == "OTPHIS") {
        $("#Agencyid").val("");
        BindOTPReport($("#Agencyid").val());
    }
}

function ResetSalesRecord() {
    $("#btnSalesReset").html("Wait..<i class='fa fa-pulse fa-spinner'></i>");
    $("#FromDate").val(""), $("#ToDate").val("");
    BindSalesReport($("#FromDate").val(), $("#ToDate").val());
    $("#btnSalesReset").html("Reset");
}
function SearchSalesReport() {
    $("#btnSalesSearch").html("Wait..<i class='fa fa-pulse fa-spinner'></i>");
    BindSalesReport($("#FromDate").val(), $("#ToDate").val());
    $("#btnSalesSearch").html("Search");
}
function SearchOTPReport() {    
    BindOTPReport($("#Agencyid").val());    
}

function BindSalesReport(fromdate, todate) {
    $.ajax({
        type: "Post",
        contentType: "application/json; charset=utf-8",
        url: "/Home/BindSalesReport",
        data: '{fromdate: ' + JSON.stringify(fromdate) + ',todate: ' + JSON.stringify(todate) + '}',
        datatype: "json",
        success: function (data) {
            if (data != null) {
                $("#BindSalesSection").html("");
                $("#BindSalesSection").html(data);
            }
        },
        failure: function (response) {
            alert("failed");
        }
    });
}

function BindAgentReport() {
    $.ajax({
        type: "Post",
        contentType: "application/json; charset=utf-8",
        url: "/Home/BindTopAgent",
        //data: '{fromdate: ' + JSON.stringify(fromdate) + ',todate: ' + JSON.stringify(todate) + '}',
        datatype: "json",
        success: function (data) {
            if (data != null) {
                $("#BindAgentSection").html("");
                $("#BindAgentSection").html(data);
            }
        },
        failure: function (response) {
            alert("failed");
        }
    });
}


function BindAirlinesReport() {
    $.ajax({
        type: "Post",
        contentType: "application/json; charset=utf-8",
        url: "/Home/BindTopAirlines",
        //data: '{fromdate: ' + JSON.stringify(fromdate) + ',todate: ' + JSON.stringify(todate) + '}',
        datatype: "json",
        success: function (data) {
            if (data != null) {
                $("#BindAirlinesSection").html("");
                $("#BindAirlinesSection").html(data);
            }
        },
        failure: function (response) {
            alert("failed");
        }
    });
}

function BindSectorReport() {
    $.ajax({
        type: "Post",
        contentType: "application/json; charset=utf-8",
        url: "/Home/BindTopSector",
        //data: '{fromdate: ' + JSON.stringify(fromdate) + ',todate: ' + JSON.stringify(todate) + '}',
        datatype: "json",
        success: function (data) {
            if (data != null) {
                $("#BindSecotrSection").html("");
                $("#BindSecotrSection").html(data);
            }
        },
        failure: function (response) {
            alert("failed");
        }
    });
}

function BindOTPReport(agencyid) {
    $.ajax({
        type: "Post",
        contentType: "application/json; charset=utf-8",
        url: "/Home/BindOTPHistory",
        data: '{agencyid: ' + JSON.stringify(agencyid) + '}',
        datatype: "json",
        success: function (data) {
            if (data != null) {
                $("#BindOTPSection").html("");
                $("#BindOTPSection").html(data);
            }
        },
        failure: function (response) {
            alert("failed");
        }
    });
}