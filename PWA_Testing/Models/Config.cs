﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace PWA_Testing.Models
{
    public static class Config
    {
        public static string MyAmdDBConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString; }
        }
    }
}