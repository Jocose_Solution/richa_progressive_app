﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PWA_Testing.Models
{
    public class ReportModel
    {
        public class ReportFilter
        {
            public string StatusType { get; set; }
            public string AgencyId { get; set; }
            public string FromDate { get; set; }
            public string ToDate { get; set; }
        }
    }
}