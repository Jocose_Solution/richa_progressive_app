﻿using PWA_Testing.Helper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using static PWA_Testing.Models.ReportModel;

namespace PWA_Testing.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult ReportPannel()
        {
            return View();
        }

        public JsonResult BindSalesReport(string fromdate, string todate)
        {
            StringBuilder sbRecord = new StringBuilder();

            try
            {
                ReportFilter filter = new ReportFilter();
                filter.StatusType = "Ticketed";

                if (string.IsNullOrEmpty(fromdate) && string.IsNullOrEmpty(todate))
                {
                    filter.FromDate = DateTime.Now.Date.ToString();
                }
                else
                {
                    filter.FromDate = fromdate;
                }
                filter.ToDate = todate;

                DataTable dtReport = ReportHelper.GetTotalAmountByAirLine(filter);

                if (dtReport != null && dtReport.Rows.Count > 0)
                {
                    for (int i = 0; i < dtReport.Rows.Count; i++)
                    {
                        sbRecord.Append("<div class='row' style='border: 1px solid #000;border-bottom: 0px;'>");
                        sbRecord.Append("<div class='col-sm-4 col-xs-4' style='padding: 5px;'><label style='font-size:11px;'><img  class='logo' src='http://b2brichatravels.in/AirLogo/sm" + dtReport.Rows[i]["vc"].ToString() + ".gif' /> -" + dtReport.Rows[i]["vc"].ToString() + "</label> </div>");
                        sbRecord.Append("<div class='col-sm-4  col-xs-4' style='border-left: 1px solid #000;padding: 5px;'><label style='font-size:10px;'>" + dtReport.Rows[i]["TicketId"].ToString() + "</label></div>");
                        sbRecord.Append(" <div class='col-sm-4  col-xs-4' style='border-left: 1px solid #000;padding: 5px;'> <label style='font-size:11px;'>" + dtReport.Rows[i]["ToatalAmount"].ToString() + "(" + dtReport.Rows[i]["Total"].ToString() + ")</label></div>");                       
                        sbRecord.Append("</div>");
                    }
                }
                else
                {
                    sbRecord.Append("<div class='row norecord'>");
                    sbRecord.Append("<div class='col-sm-12 col-xs-12'><label style='font-size: 12px;text-align: center;color: red;'>No record found !</label></div>");
                    sbRecord.Append("</div>");
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }


            return Json(sbRecord.ToString());
        }

        public JsonResult BindTopAgent()
        {
            StringBuilder sbRecord = new StringBuilder();

            try
            {               
                DataTable dtReport = ReportHelper.GetTop10AgentTotalAmount();

                if (dtReport != null && dtReport.Rows.Count > 0)
                {
                    for (int i = 0; i < dtReport.Rows.Count; i++)
                    {
                        sbRecord.Append("<div class='row' style='border: 1px solid #000;border-bottom: 0px;margin-bottom: -1px;'>");
                        sbRecord.Append(" <div class='col-sm-4  col-xs-4' style='padding: 5px;'><label style='font-size:11px;'>" + dtReport.Rows[i]["AgentId"].ToString() + "</label></div>");                        
                        sbRecord.Append(" <div class='col-sm-4  col-xs-4' style='border-left: 1px solid #000;padding: 5px;'><label style='font-size:12px;'>" + dtReport.Rows[i]["Total"].ToString() + "</label></div>");                                             
                        sbRecord.Append("  <div class='col-sm-4  col-xs-4' style='border-left: 1px solid #000;padding: 5px;'> <label style='font-size:12px;'><span style='color:red;font-weight:bold;'><span class='fa fa-inr'></span> " + dtReport.Rows[i]["ToatalAmount"].ToString() + "</span></label></div>");                       
                        sbRecord.Append("</div>");
                    }
                }
                else
                {
                    sbRecord.Append("<div class='row norecord'>");
                    sbRecord.Append("<div class='col-sm-12 col-xs-12'><label style='font-size: 12px;text-align: center;color: red;'>No record found !</label></div>");
                    sbRecord.Append("</div>");
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }


            return Json(sbRecord.ToString());
        }
        public JsonResult BindTopSector()
        {
            StringBuilder sbRecord = new StringBuilder();

            try
            {
                DataTable dtReport = ReportHelper.GetTop10sectorTotalAmount();

                if (dtReport != null && dtReport.Rows.Count > 0)
                {
                    for (int i = 0; i < dtReport.Rows.Count; i++)
                    {
                        sbRecord.Append("<div class='row' style='border: 1px solid #000;border-bottom: 0px;margin-bottom: -1px;'>");
                        sbRecord.Append(" <div class='col-sm-4  col-xs-4' style='padding: 5px;'><label style='font-size:11px;'>" + dtReport.Rows[i]["sector"].ToString() + "</label></div>");
                        sbRecord.Append(" <div class='col-sm-4  col-xs-4' style='border-left: 1px solid #000;padding: 5px;'><label style='font-size:12px;'>" + dtReport.Rows[i]["Total"].ToString() + "</label></div>");
                        sbRecord.Append("  <div class='col-sm-4  col-xs-4' style='border-left: 1px solid #000;padding: 5px;'> <label style='font-size:12px;'><span style='color:red;font-weight:bold;'><span class='fa fa-inr'></span> " + dtReport.Rows[i]["ToatalAmount"].ToString() + "</span></label></div>");
                        sbRecord.Append("</div>");                       
                    }
                }
                else
                {
                    sbRecord.Append("<div class='row norecord'>");
                    sbRecord.Append("<div class='col-sm-12 col-xs-12'><label style='font-size: 12px;text-align: center;color: red;'>No record found !</label></div>");
                    sbRecord.Append("</div>");
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }


            return Json(sbRecord.ToString());
        }
        public JsonResult BindTopAirlines()
        {
            StringBuilder sbRecord = new StringBuilder();

            try
            {
                DataTable dtReport = ReportHelper.GetTop10AirlinesTotalAmount();

                if (dtReport != null && dtReport.Rows.Count > 0)
                {
                    for (int i = 0; i < dtReport.Rows.Count; i++)
                    {
                        sbRecord.Append("<div class='row' style='border: 1px solid #000;border-bottom: 0px;margin-bottom: -1px;'>");
                        sbRecord.Append(" <div class='col-sm-4  col-xs-4' style='padding: 5px;'><label style='font-size:11px;'><img  class='logo' src='http://b2brichatravels.in/AirLogo/sm" + dtReport.Rows[i]["vc"].ToString() + ".gif' /> -" + dtReport.Rows[i]["vc"].ToString() + "</label></div>");
                        sbRecord.Append(" <div class='col-sm-4  col-xs-4' style='border-left: 1px solid #000;padding: 5px;'><label style='font-size:12px;'>" + dtReport.Rows[i]["Total"].ToString() + "</label></div>");
                        sbRecord.Append("  <div class='col-sm-4  col-xs-4' style='border-left: 1px solid #000;padding: 5px;'> <label style='font-size:12px;'><span style='color:red;font-weight:bold;'><span class='fa fa-inr'></span> " + dtReport.Rows[i]["ToatalAmount"].ToString() + "</span></label></div>");
                        sbRecord.Append("</div>");                       
                    }
                }
                else
                {
                    sbRecord.Append("<div class='row norecord'>");
                    sbRecord.Append("<div class='col-sm-12 col-xs-12'><label style='font-size: 12px;text-align: center;color: red;'>No record found !</label></div>");
                    sbRecord.Append("</div>");
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }


            return Json(sbRecord.ToString());
        }
        public JsonResult BindOTPHistory(string AgencyID)
        {
            StringBuilder sbRecord = new StringBuilder();

            try
            {
                DataTable dtReport = ReportHelper.GetOTPHistory(AgencyID);

                if (dtReport != null && dtReport.Rows.Count > 0)
                {
                    for (int i = 0; i < dtReport.Rows.Count; i++)
                    {
                        sbRecord.Append("<div class='row' style='border: 1px solid #000;border-bottom: 0px;margin-bottom: -1px;'>");
                        sbRecord.Append("<div class='col-sm-2  col-xs-2' style='padding: 5px;'><label style='font-size:11px;'>" + dtReport.Rows[i]["OTP"].ToString() + "</label></div>");
                        sbRecord.Append("<div class='col-sm-2 col-xs-2' style='border-left: 1px solid #000;padding: 5px;'> <label style='font-size:11px;'><span style='color:red;font-weight:bold;'> " + dtReport.Rows[i]["Amount"].ToString() + "</label></div>");
                        sbRecord.Append("<div class='col-sm-3  col-xs-3' style='border-left: 1px solid #000;padding: 5px;'><label style='font-size:11px;'>" + dtReport.Rows[i]["AgencyId"].ToString() + "</label></div>");                        
                        sbRecord.Append("<div class='col-sm-5  col-xs-5' style='border-left: 1px solid #000;padding: 5px;'> <label style='font-size:10px;'><span> " + dtReport.Rows[i]["AgencyName"].ToString() + "</label></div>");                       
                        sbRecord.Append("</div>");
                    }
                }
                else
                {
                    sbRecord.Append("<div class='row norecord'>");
                    sbRecord.Append("<div class='col-sm-12 col-xs-12'><label style='font-size: 12px;text-align: center;color: red;'>No record found !</label></div>");
                    sbRecord.Append("</div>");
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }


            return Json(sbRecord.ToString());
        }      
    }
}