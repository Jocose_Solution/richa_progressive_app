﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using PWA_Testing.Models;
using static PWA_Testing.Models.ReportModel;

namespace PWA_Testing.Helper
{
    public static class ReportHelper
    {
        #region [Connection Strings]
        public static SqlConnection MyAmdDBConnection = new SqlConnection(Config.MyAmdDBConnectionString);

        private static SqlCommand sqlCommand { get; set; }
        private static SqlDataAdapter sqlDataAdapter { get; set; }
        private static DataSet dataSet { get; set; }
        private static DataTable dataTable { get; set; }

        public static void MyAmdOpenConnection()
        {
            if (MyAmdDBConnection.State == ConnectionState.Closed)
            {
                MyAmdDBConnection.Open();
            }
        }
        public static void MyAmdCloseConnection()
        {
            if (MyAmdDBConnection.State == ConnectionState.Open)
            {
                MyAmdDBConnection.Close();
            }
        }
        #endregion

        public static DataTable GetTotalAmountByAirLine(ReportFilter filter)
        {
            try
            {

                sqlCommand = new SqlCommand("sp_GetTotalAmountByAirLine", MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@StatusType", (!string.IsNullOrEmpty(filter.StatusType) ? filter.StatusType : string.Empty)));
                //sqlCommand.Parameters.Add(new SqlParameter("@AgencyId", (!string.IsNullOrEmpty(filter.AgencyId) ? filter.AgencyId : null)));
                sqlCommand.Parameters.Add(new SqlParameter("@FromDate", (!string.IsNullOrEmpty(filter.FromDate) ? ConvertDateFor(filter.FromDate) : null)));
                sqlCommand.Parameters.Add(new SqlParameter("@ToDate", (!string.IsNullOrEmpty(filter.ToDate) ? ConvertDateFor(filter.ToDate) + " 23:59:59" : null)));
                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "GetTotalAmountByAirLine");
                dataTable = dataSet.Tables["GetTotalAmountByAirLine"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return dataTable;
        }

        public static DataTable GetOTPHistory(string AgencyId)
        {
            try
            {
                sqlCommand = new SqlCommand("sp_GetOTPHistory", MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;               
                sqlCommand.Parameters.Add(new SqlParameter("@AgencyId", (!string.IsNullOrEmpty(AgencyId) ? AgencyId : null)));                
                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "GetOTP");
                dataTable = dataSet.Tables["GetOTP"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return dataTable;
        }

        public static DataTable GetTop10AgentTotalAmount()
        {
            try
            {
                sqlCommand = new SqlCommand("sp_GetTop10AgentTotalAmount", MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;                
                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "GetTop10AgentTotalAmount");
                dataTable = dataSet.Tables["GetTop10AgentTotalAmount"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return dataTable;
        }

        public static DataTable GetTop10AirlinesTotalAmount()
        {
            try
            {
                sqlCommand = new SqlCommand("sp_GetTop10AirlinesTotalAmount", MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "GetTop10AirlinesTotalAmount");
                dataTable = dataSet.Tables["GetTop10AirlinesTotalAmount"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return dataTable;
        }
        public static DataTable GetTop10sectorTotalAmount()
        {
            try
            {
                sqlCommand = new SqlCommand("sp_GetTop10sectorTotalAmount", MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "GetTop10sectorTotalAmount");
                dataTable = dataSet.Tables["GetTop10sectorTotalAmount"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return dataTable;
        }


        public static string ConvertDateFor(string date)
        {
            string result = string.Empty;

            if (!string.IsNullOrEmpty(date))
            {
                DateTime dtDate = new DateTime();

                dtDate = DateTime.Parse(date);
                result = dtDate.ToString("yyyy-MM-dd");
            }

            return result;
        }
    }
}